#!/bin/bash

# Create container registry secret to allow fetching from the registry
# Replace YOUR_USERNAME_HERE, YOUR_PASSWORD_HERE with the username and password from your docker registry

# Create SSL Certificate Secret from the certificate and key
kubectl create secret tls ssl-certificate --key certs/key.pem --cert certs/cert.crt -n <namespace>
