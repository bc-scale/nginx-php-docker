#!/bin/bash

kubectl create namespace <namespace>

# Create Secrets
./create-secrets.sh

# Create Secrets
./create-db.sh

# Deploy
kubectl apply -f deploy.yaml -n <namespace>
